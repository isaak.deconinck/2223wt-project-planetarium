import { myAxios } from '@/instances/myAxios';

const fetchPlanetData = (planet) => myAxios.get(`getPlanet?name=${planet}`);

export { fetchPlanetData };