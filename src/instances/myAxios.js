import axios from 'axios';

const myAxios = axios.create({
  baseURL: 'https://planets-17f2.onrender.com/planets/'
  // see 'Request Config'
  // https://axios-http.com/docs/req_config
});

// add interceptors (optional)
// myAxios.interceptors.response.use(/* interceptors */);

// export and use in your code
export { myAxios };