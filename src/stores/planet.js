import { defineStore } from 'pinia';
import { fetchPlanetData } from '../services/planetService';
import { ref } from 'vue';

export const usePlanetStore = defineStore('planets', () => {
  const planets = ref({});

  const planetsArr = [
    'mercury',
    'venus',
    'earth',
    'mars',
    'jupiter',
    'saturn',
    'uranus',
    'neptune'
  ];

  const selectedPlanetName = ref(null);

  const getPlanetData = async (planet) => {
    fetchPlanetData(planet).then(function (response) {
      planets.value = response.data;
    });
  };

  const prevPlanet = () => {
    if (planetsArr.indexOf(selectedPlanetName.value) === 0) {
        return planetsArr[planetsArr.length - 1];
    } else {
        return planetsArr[planetsArr.indexOf(selectedPlanetName.value) - 1];
    }
  };

  const nextPlanet = () => {
    if (planetsArr.indexOf(selectedPlanetName.value) < planetsArr.length - 1) {
        return planetsArr[planetsArr.indexOf(selectedPlanetName.value) + 1];
    } else {
        return planetsArr[0];
    }
  };

  return { getPlanetData, planets, selectedPlanetName, prevPlanet, nextPlanet };
});
