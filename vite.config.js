import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue({
    template: {
      compilerOptions: {
        isCustomElement: tag => tag.
        startsWith('Tres') && tag !== 'TresCanvas',
      },
    },
  })],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "@/assets/scss/common/_breakpoints.scss";
          @import "@/assets/scss/common/_mixins.scss";
          @import "@/assets/scss/common/_vars.scss";
          @import "@/assets/scss/common/_fonts.scss";
          @import "@/assets/scss/common/_reset.scss";
      `
      }
    }
  }
});
